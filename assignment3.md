# ASSIGNMENT A3

#Client-Server

Pentru partea de Client-Server se va folosi Socket, astfel incat aplicatia v-a comunica cu serverul prin intermediul acestor Sockets pentru a putea interoga baza de date.

#Observer

In aplicatia noastra, Observerul v-a tine la curent Utilizatorul cu starea actuala a fiecarei facturi, astfel incat daca o factura isi schimba starea din "Neplatit" in "Platit", atunci clientul v-a primi o notificatie.