# ASSIGNMENT A2

# Objective
The objective of this assignment is to allow students to become familiar with the Model View Controller architectural pattern and the Factory Method design pattern.

# Application Description
Use Java/C# API to design and implement an application for bills manager. The application should have three types of users (a regular user and an administrator user and a demo user) which have to provide a username and a password in order to use the application.

The regular user can perform the following operations:

* Check and view the bills database for their own account.
* add new bill.
* delete bill.
* pay bill.
* edit their own account.
* add money to account.
* send emails to other users.



The administrator can perform the following operations:

*	CRUD on all users accounts.
*	CRUD on bills database
*	CRUD on regular users's information.
*	Generate two types of reports files, one in pdf format and one in csv format, with messages that tells if an user is out of money in account or an user have passed the time limit to pay a bill and didn't paid it.
*	create demo accounts for possible new users.
*	delete demo accounts.
*	create demo bills to show the buyers how the app works.
*	send emails to users.

The demo user or the buyer can perform the following operations:

* add a demo bill
* pay a demo bill
* delete a demo bill
* check demo bills database


# Application Constraints
*	The information about users, bills and actions will be stored in multiple XML files. Use the Model View Controller in designing the application. Use the Factory Method design pattern for generating the reports.
*	All the inputs of the application will be validated against invalid data before submitting the data and saving it.

# Requirements
*	Implement the application.
*	Test all the workflows of the application (from the controllers) using Unit Tests.

# Deliverables
*	Implementation source files.
*	Readme file that describes the installation process of the application and how to use it:
	*	how to install your application on a clean computer
	*	how to access your application and with what users

# References
General
	http://docs.oracle.com/javase/tutorial/uiswing/
	http://docs.oracle.com/javase/tutorial/jdbc/basics/index.html
	http://www.saxproject.org/sax1-roadmap.html
	http://www.roseindia.net/xml/dom/

	https://msdn.microsoft.com/en-us/library/54xbah2z(v=vs.110).aspx
	https://msdn.microsoft.com/en-us/library/e80y5yhx(v=vs.110).aspx
	http://msdn.microsoft.com/en-us/library/system.xml.xmlreader.aspx
	http://msdn.microsoft.com/en-us/library/system.xml.xmlwriter.aspx
	http://msdn.microsoft.com/en-us/library/ms764730(VS.85).aspx

Unit testing
	https://github.com/junit-team/junit4/wiki/Getting-started
	http://site.mockito.org
	http://joel-costigliola.github.io/assertj/

	https://github.com/nunit/docs/wiki/NUnit-Documentation
	https://github.com/Moq/moq4/wiki/Quickstart

