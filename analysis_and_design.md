# Project Specification
Bills manager. A software that help people to organize their lives by storing all the bills they have to pay or already payed and they can also pay bills using this software.

## Elaboration – Iteration 1.1

### Domain Model
The domain model is composed by the "Bill","Buyer", "Admin", "User", "CreateUser","DeleteUser","AdminCommands" and "Action" classes.

![diagram2](images/ConceptualClassDiagram.png)

### Architectural Design

#### Conceptual Architecture
Model–View–Controller (MVC) is a software architectural pattern for implementing user interfaces on computers. It divides a given application into three interconnected parts in order to separate internal representations of information from the ways that information is presented to and accepted from the user. The MVC design pattern decouples these major components allowing for efficient code reuse and parallel development.

#### Package Design

![diagram3](images/PackageDiagram.png)

#### Component and Deployment Diagrams

Deployment diagram:

![diagram5](images/DeploymentDiagram.png)

Component diagram:

![diagram5](images/ComponentDiagram.png)

## Elaboration – Iteration 1.2

### Design Model

#### Dynamic Behavior
[Create the interaction diagrams (1 sequence, 1 communication diagrams) for 2 relevant scenarios]

##### Sequence Diagram:
-case1: successful (paying bill)
![diagram6](images/sequence12a.png)
-case2: successful(add bill)

![diagram7](images/sequence12b.png)

Comunication Diagram:

-case1: successful (paying bill)
![diagram8](images/comunication1a.png)

-case2: successful(add bill)

![diagram9](images/comunication1b.png)



#### Class Design
[Create the UML class diagram; apply GoF patterns and motivate your choice]

I chose MVC functionality because it is simple to make it and understand it.
Also, the Observer pattern is used to notify users of their bills status.

![diagram10](images/classMVC.png)


#### Data Model
[Create the data model for the system.]

#### Unit Testing
[Present the used testing methods and the associated test case scenarios.]

## Elaboration – Iteration 2

### Architectural Design Refinement
[Refine the architectural design: conceptual architecture, package design (consider package design principles), component and deployment diagrams. Motivate the changes that have been made.]

### Design Model Refinement
[Refine the UML class diagram by applying class design principles and GRASP; motivate your choices. Deliver the updated class diagrams.]

## Construction and Transition

### System Testing
[Describe how you applied integration testing and present the associated test case scenarios.]

### Future improvements
[Present future improvements for the system]

## Bibliography
- [Markdown online editor](http://dillinger.io/)
- [Markdown documentation](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)